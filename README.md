Background of the code snippets
  - Many source code of large projects I participated are unfortunately proprietary, meaning I cannot simply post them on the internet, e.g. in a public repository.
  - I have prepared the following code snippets I wrote from a large project. The snippets are more on the general purpose side to not be considered as  proprietary. If you would like to see more details on the project, or other projects, please email me and I can share them privately.

pExpense.py
  - It is a vital part of the distributed data storage system project, because it demonstrates how to access large amount of data from the storage system which cannot typically be fetched with only one network round-trip.

pythonUtil.py
  - It is also a vital part of the distributed data storage system project, because it demonstrates how to stringently check inputs from potentially malicious sources, e.g. user input, to ensure they do not break underlying code, e.g. ML models.

stubManager.py
  - It is another vital part of the distributed data storage system project, because it demonstrates how simple load balancing can be done and it is the building block of more complex load balancing code.
