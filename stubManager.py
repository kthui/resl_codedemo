#!/usr/bin/env python3

import threading

# 
# A snippet of a class which holds reference(s) to stub(s) and allow them to be fetched in round-robin (RR) order.
# The class is thread-safe.
# 
# Background: Consider a stub as a connection to a server, in a group of servers that are identical to each other.
#             This class can act like a simple load balancer which evenly distribute loads to the group of servers.
# 
class stubManager():
  
  #
  # Constructor
  #
  def __init__(self):
    self.__stubList = []
    self.__currStubRRIndex = 0
    self.__lock = threading.RLock()
  
  #
  # Add a new stub to the stubManager.
  # 
  # Inputs
  #   stub: Any object
  #
  def newStub(self, stub):
    with self.__lock:
      self.__stubList.append(stub)   # Add stub to the end of list
    return
  
  # 
  # Set the current round-robin index (no action if the index is out of range).
  # Round-robin index refers to the order in which stub(s) are added.
  # 
  # Inputs
  #   stubRRIndex: Round-robin index to be set, should be >= 0 and < number of stub(s) added [int]
  # 
  def setStubRRIndex(self, stubRRIndex):
    with self.__lock:
      if stubRRIndex < 0 or stubRRIndex >= len(self.__stubList):   # Ensure stubRRIndex is within range
        return
      self.__currStubRRIndex = stubRRIndex   # Set the current round-robin index to stubRRIndex
    return
  
  #
  # Return the next stub based on round-robin if stubRRIndex is not provided.
  # If stubRRIndex is provided, set the current round-robin index to it and return the specified stub.
  # Round-robin index refers to the order in which stub(s) are added.
  # 
  # Inputs
  #   stubRRIndex: Round-robin index to be set, should be >= 0 and < number of stub(s) added [int]
  # 
  # Outputs
  #   Return: None if no stub has been added, or stubRRIndex is not None and out of range.
  #           Otherwise, either the next stub based on round-robin or the specified stub.
  # 
  # Remark: Avoid calling setStubRRIndex(newIndex - 1) and then getStub() to prevent concurrency issue. 
  #         Instead, call getStub(newIndex) directly.
  #
  def getStub(self, stubRRIndex = None):
    with self.__lock:
      if len(self.__stubList) <= 0:   # Check if no stub has been added
        return None
      if stubRRIndex == None:
        # stubRRIndex is not provided
        self.__currStubRRIndex = (self.__currStubRRIndex + 1) % len(self.__stubList)   # Round-robin to the next stub as the current index
      else:
        # stubRRIndex is provided (It is faster this way than calling self.setStubRRIndex())
        if stubRRIndex < 0 or stubRRIndex >= len(self.__stubList):   # Ensure stubRRIndex is within range
          return None
        self.__currStubRRIndex = stubRRIndex   # Set the current round-robin index to stubRRIndex
      return self.__stubList[self.__currStubRRIndex]
    return

