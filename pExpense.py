#!/usr/bin/env python3

from datetime import datetime
from datetime import timedelta

#
# A snippet of a class which demonstrates how to break a lengthy download into multiple smaller downloads and then combine them
# 
# Background: The expenses are stored on remote servers over the internet. Each call to the servers can only download a limited number 
#             of expenses before exceeding the timeout period, so the function below breaks a lengthy download into multiple smaller 
#             downloads to prevent timeout errors.
#
class pExpense():
  
  #
  # Return a list of expense of any length storable in the memory
  # 
  # Inputs
  #   gC:               An object for obtaining a smaller list of expense over the internet
  #   siteName:         Name of the site which the list of expense should be fetched [str]
  #   userId:           The user id of the user which the list of expense shoud be fetched [int (uint32)]
  #   endTime:          Fetch expenses which its "purchaseDate" is less than the endTime [datetime]
  #   countBefore:      Number of expenses to be fetched before the endDate (Use 0 to fetch all) [int (uint)]
  #   waitSec:          The number of seconds the caller is willing to wait before the function should return whatever is fetched. 
  #                       The function will execute for at least the specified seconds (Use 0 for unlimited waiting time) [int (uint)]
  #   skipIsDelete:     True to skip mark deleted expenses, False to retain the expenses 
  #                       (skipped expenses do not count towards countBefore)
  #   loadReceiptImage: True to include receipt image of each expense (if available), False to not include image
  # 
  # Outputs
  #   Return: None if errors occured, otherwise a list of expense (see data storage documentation for items in expense)
  #
  @staticmethod
  def __getExpenseArray(gC, siteName, userId, endTime, countBefore = 0, waitSec = 0, skipIsDelete = False, loadReceiptImage = True):
    
    # Variables
    fnStartTime = datetime.utcnow()   # Start time of the function, to enforce waitSec
    endTime_ = endTime   # End time of each iteration of expense list fetch
    expenseList = []   # A list to store expense fetched
    winSize = 8 if loadReceiptImage else 32   # The number of expenses to be fetched for each iteration (will be tuned during fetch)
    
    # Fetch loop
    while True:
      # Obtain the winSize number of expenses before the endTime_
      result = gC.DataStore.GetExpenseArray({"mode": (2 if loadReceiptImage else 3), "count": -winSize, 
                                             "array": [{"userId": userId, "purchaseDate": endTime_}]}, siteName)
      # Check if obtain is successful with the current winSize
      if result == None:
        winSize = int(winSize / 2)   # Reduce the winSize by half (TODO: Less aggressive reduction might aid a smoother download)
        if winSize < 1:
          return None   # Return error, as download failed with the smallest winSize (maybe bad connection?)
        continue
      # Focus only on the expense list from the download
      result = result["array"]
      # Inspect the download
      i = 0
      for r in result:
        # Remove duplicated download if the window cuts off in between 2 expenses with the same "purchaseDate"
        #   (TODO: The runtime can be better, but this one is good enough as it is simple and the search list is short)
        if r["purchaseDate"] >= (endTime_ - timedelta(seconds=1)):
          isRepeat = False
          for e in expenseList:
            if e["purchaseDate"] > r["purchaseDate"]:
              break
            if e["idG"] == r["idG"]:
              isRepeat = True
              break
          if isRepeat:
            continue
        # Insert not deleted expenses only if skipIsDelete is set
        if not (skipIsDelete and r["isDelete"]):
          expenseList.insert(i, r)
          i = i + 1
      # If 1 or more expense is downloaded, move the endTime_ earliest expense for the next iteration
      if len(result) > 0:
        endTime_ = result[0]["purchaseDate"] + timedelta(seconds=1)   # Add 1 second incase there are expenses with the same "purchaseDate"
      # Terminate the download if
      #   - there are less expenses returned than the winSize -> download is complete with no more expenses available
      #   - countBefore is set, and the number of expenses downloaded has reached or exceeded the countBefore
      #   - waitSec is set, and the total execution time has exceeded the waitSec
      if (len(result) < winSize or (countBefore > 0 and len(expenseList) >= countBefore) or 
                                   (waitSec > 0 and len(expenseList) > 0 and (datetime.utcnow() - fnStartTime).total_seconds() > waitSec)):
        break
      # Slowly increase the winSize for each successful download
      winSize = winSize + 1
    
    # Return
    return expenseList[-countBefore:] if countBefore > 0 else expenseList   # Return exactly countBefore if it is set and expenseList is longer
  

