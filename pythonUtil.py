#!/usr/bin/env python3

# To use: from pythonUtil import strIsNumber

from datetime import datetime

#
# Checks whether a string is representing a number strictly
# 
class strIsNumber():
  
  #
  # Check if the input is a str and it represents an Int32 [-2147483648, 2147483648)
  # 
  # Inputs
  #   s: Any python variable (will not be modified)
  #
  # Outputs
  #   Return: True if s is a str and it represents an Int32, otherwise False
  #
  @staticmethod
  def isInt(s):
    if not (isinstance(s, str) and len(s) > 0 and len(s) <= 11):
      return False
    for i in range(0, len(s)):
      if not (s[i] == "0" or s[i] == "1" or s[i] == "2" or s[i] == "3" or s[i] == "4" or 
              s[i] == "5" or s[i] == "6" or s[i] == "7" or s[i] == "8" or s[i] == "9" or 
              (i == 0 and s[i] == "-" and len(s) > 1)):
        return False
    i = int(s)
    if i >= -2147483648 and i < 2147483648:
      return True
    return False
  
  #
  # Check if the input is a str and it represents an UInt32 [0, 4294967296)
  # 
  # Inputs
  #   s: Any python variable (will not be modified)
  #
  # Outputs
  #   Return: True if s is a str and it represents an UInt32, otherwise False
  #
  @staticmethod
  def isUInt(s):
    if not (isinstance(s, str) and len(s) > 0 and len(s) <= 10):
      return False
    for i in range(0, len(s)):
      if not (s[i] == "0" or s[i] == "1" or s[i] == "2" or s[i] == "3" or s[i] == "4" or 
              s[i] == "5" or s[i] == "6" or s[i] == "7" or s[i] == "8" or s[i] == "9"):
        return False
    if int(s) < 4294967296:
      return True
    return False
  
  #
  # Check if the input is a str and it represents an integer or float
  # !!! Beware of this function does not check for precision nor value range !!!
  # 
  # Inputs
  #   s: Any python variable (will not be modified)
  #
  # Outputs
  #   Return: True if s is a str and it represents an integer or float, otherwise False
  #
  @staticmethod
  def isFloat(s):
    if not (isinstance(s, str) and len(s) > 0):
      return False
    hasDot = False
    for i in range(0, len(s)):
      if not (s[i] == "0" or s[i] == "1" or s[i] == "2" or s[i] == "3" or s[i] == "4" or 
              s[i] == "5" or s[i] == "6" or s[i] == "7" or s[i] == "8" or s[i] == "9" or 
              (i == 0 and s[i] == "-" and len(s) > 1)):
        if i > 0 and s[i] == "." and i < (len(s) - 1) and hasDot == False:
          hasDot = True
          continue
        return False
    return True
  
  #
  # Check if the input is a str and it represents a boolean
  # "True", "true", "1", "False", "false", "0" are all the string representations that this function will recognize
  # 
  # Inputs
  #   s: Any python variable (will not be modified)
  #
  # Outputs
  #   Return: True if s is a str and it represents an boolean, otherwise False
  #
  @staticmethod
  def isBool(s):
    if not (isinstance(s, str) and len(s) > 0):
      return False
    if s == "True" or s == "true" or s == "1" or s == "False" or s == "false" or s == "0":
      return True
    return False
  
  #
  # Check if the input is a str and it represents a date in "YYYY-MM-DD" format
  # 
  # Inputs
  #   s: Any python variable (will not be modified)
  #
  # Outputs
  #   Return: True if s is a str and it represents the above date and time format, otherwise False
  #
  @staticmethod
  def isDate(s):
    if not (isinstance(s, str) and len(s) == 10):
      return False
    try:
      d = datetime.strptime(s, "%Y-%m-%d")
    except:
      return False
    return True
  
  #
  # Check if the input is a str and it represents a date and time
  # !!! Only "YYYY-MM-DDTHH:MM:SSZ" UTC format in ISO 8601 is recognized !!!
  # 
  # Inputs
  #   s: Any python variable (will not be modified)
  #
  # Outputs
  #   Return: True if s is a str and it represents the above date and time format, otherwise False
  #
  @staticmethod
  def isDateTime_ISO8601(s):
    if not (isinstance(s, str) and len(s) == 20):
      return False
    try:
      d = datetime.strptime(s, "%Y-%m-%dT%H:%M:%SZ")
    except:
      return False
    return True

